package lopezyandri.facci.prueba.sqlmia;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import lopezyandri.facci.prueba.sqlmia.utilidades.UtilidadesEstudiante;

public class RegistrarConsultaActivity extends AppCompatActivity {
    EditText editTextIdRegistrar,editTextNombre,editTextNivel,editTextCarrera;
    Button buttonRegistrar,buttonEliminar,buttonActualizar;
    ConexionSQLiteHelper conn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrar_consulta);
        conn = new ConexionSQLiteHelper(this,"bd estudiantes",null,1);
        editTextNivel = (EditText)findViewById(R.id.editTextNivel);
        editTextIdRegistrar = (EditText)findViewById(R.id.editTextId);
        editTextNombre=(EditText)findViewById(R.id.editTextNombre);
        editTextCarrera=(EditText)findViewById(R.id.editTextCarrera);
        buttonRegistrar=(Button)findViewById(R.id.buttonRegistrar);
        buttonActualizar=(Button)findViewById(R.id.buttonActualizar);
        buttonEliminar=(Button)findViewById(R.id.buttonEliminar);

        buttonRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registrarEstudiante();
            }
        });

        buttonActualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                actualizarEstudiante();
            }
        });

        buttonEliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                eliminarEstudiante();
            }
        });

    }

    private void eliminarEstudiante() {
        SQLiteDatabase db = conn.getWritableDatabase();
        String[] parametro = {editTextIdRegistrar.getText().toString()};
        db.delete(UtilidadesEstudiante.TABLA_ESTUDIANTE,UtilidadesEstudiante.CAMPO_ID+"=?",parametro);
        Toast.makeText(getApplicationContext(),"Eliminarar Estudiante ",Toast.LENGTH_LONG).show();
        db.close();


    }

    private void actualizarEstudiante() {
        SQLiteDatabase db = conn.getWritableDatabase();
        String[] parametro = {editTextIdRegistrar.getText().toString()};
        ContentValues values = new ContentValues();
        values.put(UtilidadesEstudiante.CAMPO_NOMBRE,editTextNombre.getText().toString());
        values.put(UtilidadesEstudiante.CAMPO_CARRERA,editTextCarrera.getText().toString());
        values.put(UtilidadesEstudiante.CAMPO_NIVEL,editTextNivel.getText().toString());
        db.update(UtilidadesEstudiante.TABLA_ESTUDIANTE,values,UtilidadesEstudiante.CAMPO_ID+"=?",parametro);
        Toast.makeText(getApplicationContext(),"Actualizar Estudiante ",Toast.LENGTH_LONG).show();
        db.close();

    }

    private void registrarEstudiante() {
        SQLiteDatabase db = conn.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(UtilidadesEstudiante.CAMPO_ID,editTextIdRegistrar.getText().toString());
        values.put(UtilidadesEstudiante.CAMPO_NOMBRE,editTextNombre.getText().toString());
        values.put(UtilidadesEstudiante.CAMPO_CARRERA,editTextCarrera.getText().toString());
        values.put(UtilidadesEstudiante.CAMPO_NIVEL,editTextNivel.getText().toString());
        Long idResultatnte= db.insert(UtilidadesEstudiante.TABLA_ESTUDIANTE,UtilidadesEstudiante.CAMPO_ID,values);
        Toast.makeText(getApplicationContext(),"Registro id:"+idResultatnte,Toast.LENGTH_LONG).show();
        db.close();


    }
}
