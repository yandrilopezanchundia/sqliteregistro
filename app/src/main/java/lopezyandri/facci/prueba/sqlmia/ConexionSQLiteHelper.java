package lopezyandri.facci.prueba.sqlmia;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import lopezyandri.facci.prueba.sqlmia.utilidades.UtilidadesEstudiante;

public class ConexionSQLiteHelper extends SQLiteOpenHelper {
   // final String CREAR_TABLA_ESTUDIANTE ="CREATE TABLE estudiantes (id INTEGER, nombre TEXT, carrera TEXT,nivel TEXT)";
    public ConexionSQLiteHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(UtilidadesEstudiante.CREAR_TABLA_ESTUDIANTE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS estudiantes");
        onCreate(db);
    }
}
