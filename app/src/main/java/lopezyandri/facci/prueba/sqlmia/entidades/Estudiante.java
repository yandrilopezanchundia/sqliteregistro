package lopezyandri.facci.prueba.sqlmia.entidades;

public class Estudiante {
    private Integer id;
    private String nombre;
    private String carrera;
    private String nivel;


    public Estudiante(Integer id, String nombre, String carrera, String nivel) {
        this.id = id;
        this.nombre = nombre;
        this.carrera = carrera;
        this.nivel = nivel;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCarrera() {
        return carrera;
    }

    public void setCarrera(String carrera) {
        this.carrera = carrera;
    }

    public String getNivel() {
        return nivel;
    }

    public void setNivel(String nivel) {
        this.nivel = nivel;
    }
}
