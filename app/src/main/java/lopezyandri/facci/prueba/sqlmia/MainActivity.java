package lopezyandri.facci.prueba.sqlmia;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import lopezyandri.facci.prueba.sqlmia.utilidades.UtilidadesEstudiante;

public class MainActivity extends AppCompatActivity {
    EditText editTextCedula;
    Button buttonCedula;
    TextView textViewCedula,textViewNombre,textViewCarrera,textViewNivel;


    ConexionSQLiteHelper conn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        conn = new ConexionSQLiteHelper(this,"bd estudiantes",null,1);
        editTextCedula = (EditText)findViewById(R.id.editTextCedula);
        buttonCedula = (Button)findViewById(R.id.buttonCedula);
        textViewCarrera = (TextView)findViewById(R.id.textViewCarrera);
        textViewCedula = (TextView)findViewById(R.id.textViewCedula);
        textViewNivel = (TextView)findViewById(R.id.textViewNivel);
        textViewNombre = (TextView)findViewById(R.id.textViewNombre);

        buttonCedula.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                consultar();
            }
        });

    }

    private void consultar() {
        SQLiteDatabase db = conn.getReadableDatabase();
        String[] parametro = {editTextCedula.getText().toString()};
        String[] campo = {UtilidadesEstudiante.CAMPO_ID,UtilidadesEstudiante.CAMPO_NOMBRE,
                UtilidadesEstudiante.CAMPO_CARRERA,UtilidadesEstudiante.CAMPO_NIVEL};

        try{
            Cursor cursor = db.query(UtilidadesEstudiante.TABLA_ESTUDIANTE,campo,UtilidadesEstudiante.CAMPO_ID+"=?",parametro,null,null,null);
            cursor.moveToFirst();
            textViewCedula.setText(cursor.getString(0));
            textViewNombre.setText(cursor.getString(1));
            textViewCarrera.setText(cursor.getString(2));
            textViewNivel.setText(cursor.getString(3));
            cursor.close();
        }catch (Exception e){
            Toast.makeText(getApplicationContext(),"El id no existe",Toast.LENGTH_LONG).show();
            Intent intent = new Intent(MainActivity.this,RegistrarConsultaActivity.class);
            startActivity(intent);
        }
        db.close();

    }
}
