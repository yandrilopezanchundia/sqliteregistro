package lopezyandri.facci.prueba.sqlmia.utilidades;

public class UtilidadesEstudiante {
    public static  final String TABLA_ESTUDIANTE ="estudiante";
    public static  final String CAMPO_ID ="id";
    public static  final String CAMPO_NOMBRE ="nombre";
    public static  final String CAMPO_CARRERA ="carrera";
    public static  final String CAMPO_NIVEL ="nivel";

    public static  final String CREAR_TABLA_ESTUDIANTE ="CREATE TABLE "+
            TABLA_ESTUDIANTE+" ("+
            CAMPO_ID+" INTEGER, "+
            CAMPO_NOMBRE+" TEXT, "+
            CAMPO_CARRERA+" TEXT, "+
            CAMPO_NIVEL+" TEXT)";
}
